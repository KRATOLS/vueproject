export const mainContextMenu = [
    {
        "type": "single",
        "action": "callModal(null, 'edit')",
        "text": "Редактировать",
    },
    {
        "type": "single",
        "action": "insertRow()",
        "text": "Добавить строку",
    },
    {
        "type": "single",
        "action": "removeRow()",
        "text": "Удалить строку",
    },
    {
        "type": "single",
        "action": "copyCellInner()",
        "text": "Копировать",
    },
    {
        "type": "single",
        "action": "insertCellInner()",
        "text": "Вставить"
    },
    {
        "type": "multy",
        "action": null,
        "text": "Сортировать"
    }
]
export const firstChildContextMenu = [
    {
        "type": "single",
        "action": "desc",
        "text": "Сортировать по убыванию",
    },
    {
        "type": "single",
        "action": "asc",
        "text": "Сортировать по возрастанию",
    }
]
export const selectBoxValues = [
    {
        "value": 2,
        "text": "По умолчанию",
    },
    {
        "value": 5,
        "text": "По 5",
    },
    {
        "value": 10,
        "text": "По 10"
    },
    {
        "value": "all",
        "text": "Все"
    }
]
export const testCallsData = [
    {
        id: 1,
        call_id: { value: "4tm-1122121.12", isSelected: false, isEdited: false, type: "String", name: "Ид звонка"  },
        comment: { value: "Ошибочный результат", isSelected: false, isEdited: false, type: "String", name: "Комментарий к звонку"  },
        abonent: { value: "Максим Золоторёв", isSelected: false, isEdited: false, type: "String", name: "Абонент"  },
        call_time: { value: "02:24", isSelected: false, isEdited: false, type: "Time", name: "Длительность"  }
    },
    {
        id: 2,
        call_id: { value: "4tm-1122121.13", isSelected: false, isEdited: false, type: "String", name: "Ид звонка"  },
        comment: { value: "Первый звонок", isSelected: false, isEdited: false, type: "String", name: "Комментарий к звонку"  },
        abonent: { value: "Петрова Василиса", isSelected: false, isEdited: false, type: "String", name: "Абонент"  },
        call_time: { value: "02:18", isSelected: false, isEdited: false, type: "Time", name: "Длительность"  }
    },
    {
        id: 3,
        call_id: { value: "4tm-1122121.14", isSelected: false, isEdited: false, type: "String", name: "Ид звонка"  },
        comment: { value: "Звонок другу", isSelected: false, isEdited: false, type: "String", name: "Комментарий к звонку"  },
        abonent: { value: "Абрамов Дмитрий", isSelected: false, isEdited: false, type: "String", name: "Абонент"  },
        call_time: { value: "02:18", isSelected: false, isEdited: false, type: "Time", name: "Длительность"  }
    },
    {
        id: 4,
        call_id: { value: "4tm-1122121.15", isSelected: false, isEdited: false, type: "String", name: "Ид звонка"  },
        comment: { value: "Второй звонок", isSelected: false, isEdited: false, type: "String", name: "Комментарий к звонку"  },
        abonent: { value: "Лаврова Любовь", isSelected: false, isEdited: false, type: "String", name: "Абонент"  },
        call_time: { value: "02:18", isSelected: false, isEdited: false, type: "Time", name: "Длительность"  }
    },
    {
        id: 5,
        call_id: { value: "4tm-1122121.16", isSelected: false, isEdited: false, type: "String", name: "Ид звонка"  },
        comment: { value: "Тестовый", isSelected: false, isEdited: false, type: "String", name: "Комментарий к звонку"  },
        abonent: { value: "Степанов Денис", isSelected: false, isEdited: false, type: "String", name: "Абонент"  },
        call_time: { value: "02:18", isSelected: false, isEdited: false, type: "Time", name: "Длительность"  }
    },
    {
        id: 6,
        call_id: { value: "4tm-1122121.17", isSelected: false, isEdited: false, type: "String", name: "Ид звонка"  },
        comment: { value: "Ещё один звонок", isSelected: false, isEdited: false, type: "String", name: "Комментарий к звонку"  },
        abonent: { value: "Новикова Анастасия", isSelected: false, isEdited: false, type: "String", name: "Абонент"  },
        call_time: { value: "01:26", isSelected: false, isEdited: false, type: "Time", name: "Длительность"  }
    },
    {
        id: 7,
        call_id: { value: "4tm-1122121.18", isSelected: false, isEdited: false, type: "String", name: "Ид звонка"  },
        comment: { value: "Тестовый", isSelected: false, isEdited: false, type: "String", name: "Комментарий к звонку"  },
        abonent: { value: "Степанов Денис", isSelected: false, isEdited: false, type: "String", name: "Абонент"  },
        call_time: { value: "02:12", isSelected: false, isEdited: false, type: "Time", name: "Длительность"  }
    },
    {
        id: 8,
        call_id: { value: "4tm-1122121.19", isSelected: false, isEdited: false, type: "String", name: "Ид звонка"  },
        comment: { value: "Некорректный результат звонка", isSelected: false, isEdited: false, type: "String", name: "Комментарий к звонку"  },
        abonent: { value: "Алымов Максим", isSelected: false, isEdited: false, type: "String", name: "Абонент"  },
        call_time: { value: "02:18", isSelected: false, isEdited: false, type: "Time", name: "Длительность"  }
    },
    {
        id: 9,
        call_id: { value: "4tm-1122121.20", isSelected: false, isEdited: false, type: "String", name: "Ид звонка"  },
        comment: { value: "Неправильное ударение в имени клиента", isSelected: false, isEdited: false, type: "String", name: "Комментарий к звонку"  },
        abonent: { value: "Громова Лилия", isSelected: false, isEdited: false, type: "String", name: "Абонент"  },
        call_time: { value: "03:45", isSelected: false, isEdited: false, type: "Time", name: "Длительность"  }
    },
    {
        id: 10,
        call_id: { value: "4tm-1122121.21", isSelected: false, isEdited: false, type: "String", name: "Ид звонка"  },
        comment: { value: "Новый звонок", isSelected: false, isEdited: false, type: "String", name: "Комментарий к звонку"  },
        abonent: { value: "Сосновский Виталий", isSelected: false, isEdited: false, type: "String", name: "Абонент"  },
        call_time: { value: "00:34", isSelected: false, isEdited: false, type: "Time", name: "Длительность"  }
    },
]
export const requiredCells = ['call_id', 'abonent', 'call_time'];
export const uniqueValues = ['call_id'];
export const validDataType = ['call_time'];