import Vue from 'vue';
import VueRouter from 'vue-router';

import CallsTable from './components/CallsTable.vue';
import NotFound from './components/NotFound.vue';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: CallsTable },
        { path: '/calls/table', component: CallsTable },
        { path: '*', component: NotFound }
    ]
})

export default router