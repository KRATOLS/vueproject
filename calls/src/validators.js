let validators = {
     validRequiredCells(requiredCells, row){
        for(let cell of requiredCells){
            if(row[cell]['value'] === '' || row[cell]['value'] === null){
                return `Не заполнено обязательное поле ${cell}`;
            }
        }
        return null;
    },
    validUniqueValues(uniqueValues, sourceData, row){
            for(let cell of uniqueValues){
                if(sourceData.filter(item => item[cell]['value'] === row[cell]['value'] && item.id !== row.id).length !== 0){
                    return `Строка с указанным значением ${cell} - ${row[cell]['value']} уже существует`;
                } 
            }
            return null;
    },
    validTypes(validDataType, row){
        let validTime = function(value){
            let re = /^\d\d:\d\d$/;

            if(value.match(re) === null){
                return `Некорректный формат времени, допускается: mm:ss`;
            }
            return null;
        }

        for(let cell of validDataType){
            switch(row[cell]['type']){
                case 'Time':
                    return validTime(row[cell]['value']);
                default:
                    return
            }
        }
    },
}

export default validators;